from mock import MagicMock
from jobs.etl import fix_malformed

def test_to_pairs():

	spark = MagicMock()
    df = spark.createDataFrame([("Kelly O'Connel",)], ['f'])
    result = fix_malformed(df, 'f')

    assert result.select('f').collect()[0]['f'] == 'Kelly OConnel'

