from pyspark.ml.feature import OneHotEncoder, StringIndexer
import pyspark.sql.functions as F
from pyspark.sql.types import ArrayType
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import LongType
from pyspark.sql.types import StringType
from pyspark.sql.types import IntegerType
from pyspark.sql.types import BooleanType
from pyspark.sql.types import BooleanType

class Transformer(object):


	def __init__(self, spark, df):

		self.df = df
		self.spark = spark
		print('self.spark = {0}'.format(spark))



	def extract_from_json_field(self, field_name, tmp_table_name, *args, **kwargs):
    
		self.df.registerTempTable(tmp_table_name)
		print('tempTable {0} is registered on df'.format(tmp_table_name))
		
		df_for_schema = self.spark.sql("select {field_name} from {tmp_table_name} limit 1" \
								  .format(field_name=field_name, tmp_table_name=tmp_table_name))
		print('df_for_schema = {0}'.format(df_for_schema))
		
		jsonString = df_for_schema.collect()[0][field_name]
		print('jsonString = {0}'.format(jsonString))
		
		df_for_schema = self.spark.read.json(self.spark.sparkContext.parallelize([jsonString]))
		print('df_for_schema.SCHEMA = {0}'.format(df_for_schema.schema))
		
		df_extracted = self.spark.sql("select id, {field_name} from {tmp_table_name}" \
							  .format(field_name=field_name, tmp_table_name=tmp_table_name)) \
								.withColumn('f_json', F.from_json(F.col(field_name), ArrayType(df_for_schema.schema)))
		print('created target df {0}'.format(df_extracted))
		
		df_extracted = df_extracted.withColumn('f_exploded', F.explode('f_json'))
		print('exploded')
		
		fields = df_for_schema.schema.jsonValue()['fields']
		print('fields: {0}'.format(fields))
		
		field_prefix = field_name if 'field_prefix' not in kwargs else kwargs['field_prefix']
		for f in fields:
			df_extracted = df_extracted.withColumn('{0}_{1}' \
							.format(field_prefix, f['name']), F.col('f_exploded').getItem(f['name']))
		
		df_extracted = df_extracted.drop(field_name).drop('f_exploded').drop('f_json')
		print('df_extracted.columns: {0}'.format(df_extracted.columns))
		
		return df_extracted



	def json_to_categorical(self, from_column, json_column, to_column, drop_old = True):
	
		df = self.extract_from_json_field(from_column, 'movies_metadata')
		
		df.registerTempTable(from_column)
		self.df.registerTempTable('metadata')
		
		self.df = self.spark.sql('select t1.{from_column}_{json_column} as {to_column}, t0.* \
									from \
										metadata t0 \
										left join {from_column} t1 \
										on t1.id = t0.id \
									'.format(from_column=from_column, json_column=json_column, to_column=to_column)
			   ).fillna('na', [to_column])

		return self



	def json_to_pivot(self, from_column, groupby_column, pivot_column):
	
		df = self.extract_from_json_field(from_column, 'movies_metadata')
		
		new_id = 'movie_{0}'.format(groupby_column)
		df = df.withColumn(new_id, F.col(groupby_column)).drop(groupby_column)
		df = df.fillna('na', [pivot_column])
		
		df_pivot = df.groupby(new_id) \
						.pivot('{from_column}_{pivot_column}'.format(from_column=from_column, pivot_column=pivot_column)) \
						.count() \
						.fillna(0)
		
		cols = df_pivot.columns
		cols.remove(new_id)
		print('pivot_columns: {0}'.format(cols))

		col_prefix = from_column 
		for c in cols:
			df_pivot = df_pivot.withColumn('{0}_{1}'.format(col_prefix, c), F.col(c)).drop(c)

		df_pivot.registerTempTable(from_column)
		self.df.registerTempTable('metadata')
		
		self.df = self.spark.sql('select t1.*, t0.* \
								from \
									metadata t0 \
									left join {0} t1 \
									on t1.{1} = t0.id \
							'.format(from_column, new_id)) \
							.drop(new_id)
		
		return self

		
	def encode_categorical(self, column_name, encoder = OneHotEncoder, replace = True):
	
		self.df = self.df.fillna('na', [column_name])
		stringIndexer = StringIndexer(inputCol=column_name, outputCol="{0}_index".format(column_name))
		model = stringIndexer.fit(self.df)
		self.df = model.transform(self.df)

		encoder = OneHotEncoder(inputCol="{0}_index".format(column_name), outputCol="{0}_vec".format(column_name))
		self.df = encoder.transform(self.df).drop("{0}_index".format(column_name))
		
		if(replace):
			self.df = self.df.drop(column_name)
		
		return self
	
	
	def toDF(self):
	
		return self.df