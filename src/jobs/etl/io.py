
class DataFrameIO(object):

	def __init__(self, spark, work_dir=''):
	
		self.spark = spark
		self.work_dir = work_dir


	def from_csv(self, file_name):
	
		return self.spark.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load("{0}/{1}".format(self.work_dir, file_name))
	
	
	def to_parquet(self, file_name):
	
		self.spark.write.parquet("{0}/{1}".format(self.work_dir, file_name))