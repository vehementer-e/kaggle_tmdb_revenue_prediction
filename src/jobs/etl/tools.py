import pyspark.sql.functions as F

def fix_malformed(df, json_cols):

	malform_fixes = [
						{	'from' : "(\w)'(\w)", 	'to' : '$1$2'	},
						{	'from' : "\sNone", 		'to' : "'None'"	}
					]

	for c in json_cols:
		for fix in malform_fixes:
			df = df.withColumn(c, F.regexp_replace(c, fix['from'], fix['to']))
	
	return df
