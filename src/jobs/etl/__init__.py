import pyspark
from pyspark.sql.functions import from_json
from pyspark.sql.window import Window
from pyspark.sql import Row

from pyspark.ml.feature import OneHotEncoder, StringIndexer

from jobs.etl.io import DataFrameIO
from jobs.etl.tools import fix_malformed
from jobs.etl.transform import Transformer


WORK_DIR = '/tmp/tmdb'
METADATA_SRC_FILE = 'movies_metadata_fixed_dq_over.csv'


def execute(spark, save_intermediate = False):

	print("preparing train and test dataframes")
	
#	
	
	io 		= 	DataFrameIO(spark, work_dir=WORK_DIR)
	df_src 	= 	io.from_csv(METADATA_SRC_FILE)
	df_src.printSchema()
	
	json_cols = ['belongs_to_collection', 'genres', 'production_companies', 'production_countries', 'spoken_languages']
	df_src = fix_malformed(df_src, json_cols)

	transformer = Transformer(spark, df_src)
	
	transformer \
		.json_to_categorical('belongs_to_collection', json_column = 'name', to_column = 'cln_name') \
		.json_to_pivot('genres', groupby_column='id', pivot_column='id') \
		.json_to_pivot('production_countries', groupby_column='id', pivot_column='iso_3166_1') \
		.json_to_pivot('spoken_languages', groupby_column='id', pivot_column='iso_639_1')

	
	categorical_cols = ['status', 'original_language', 'cln_name']
	for c in categorical_cols:
		transformer.encode_categorical(c) 
	

	transformed = transformer.toDF()
	transformed.printSchema()
	
	
	
